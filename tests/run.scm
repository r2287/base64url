(import (chicken format)
        srfi-1
        test
        base64url)


(test-group "Check character table"
  (let ([upper (zip (map integer->char (iota 26 (char->integer #\A))) (iota 26))]
        [lower (zip (map integer->char (iota 26 (char->integer #\a))) (iota 26 26))]
        [nums (zip (map integer->char (iota 10 (char->integer #\0))) (iota 10 52))]
        [special '((#\- 62) (#\_ 63))])
    (for-each 
      (lambda (x) 
        (test 
          (sprintf "Char at ~a = ~a" (cadr x) (car x)) 
          (car x) 
          (base64url-character-table (cadr x)))) 
      (append upper lower nums special))
    (for-each 
      (lambda (x) 
        (test 
          (sprintf "Index for ~a = ~a" (car x) (cadr x)) 
          (cadr x) 
          (base64url-index-table (car x)))) 
      (append upper lower nums special))))

(test "" (repeat/char #\a 0))
(test "a" (repeat/char #\a 1))
(test "xxxxxxxxx" (repeat/char #\x 9))

(test "" (string->binary ""))
(test "01000001" (string->binary "A"))
(test "111011001001101110101000" (string->binary "웨"))

(test '() (group-per 6 '()))
(test '((1 2 3 4 5 6) (7 8 9 10 11 12)) (group-per 6 '(1 2 3 4 5 6 7 8 9 10 11 12)))
(test '((1 2 3 4 5 6) (7 8 9 10 11 12) (13 #\0 #\0 #\0 #\0 #\0)) (group-per 6 '(1 2 3 4 5 6 7 8 9 10 11 12 13)))

(test '() (prepend/2 '()))
(test '((#\0 #\0 1 2 3) (#\0 #\0 4 5)) (prepend/2 '((1 2 3) (4 5))))

(test "00000000" (integer->binary 0))
(test "00010000" (integer->binary 16))
(test "00000011" (integer->binary 3))

(test '("") (strip-first/2 '("")))
(test '("") (strip-first/2 '("0")))
(test '("") (strip-first/2 '("11")))
(test '("010000") (strip-first/2 '("00010000")))
(test '("010000" "345678") (strip-first/2 '("00010000" "12345678")))

(test "QUJD" (b64encode "ABC"))

(test "ABC" (b64decode "QUJD"))

(test "Longer thing to encode" (b64decode (b64encode "Longer thing to encode")))

(test-exit)
