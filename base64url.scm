(module base64url * 
(import 
  scheme 
  (chicken base)
  (chicken format)
  (chicken string)
  srfi-1
  srfi-69)

(define base64url-table
  ((lambda ()
    (let* ([table (make-hash-table)]
            [index (make-hash-table)]
            [char (make-hash-table)]
            [set (lambda (x y) (hash-table-set! table x y))]
            [setI (lambda (x) (hash-table-set! index (car x) (cdr x)))]
            [setC (lambda (x) (hash-table-set! char (cdr x) (car x)))]
            [values '( 
            ( 0 . #\A) ( 1 . #\B) ( 2 . #\C) ( 3 . #\D) ( 4 . #\E) 
            ( 5 . #\F) ( 6 . #\G) ( 7 . #\H) ( 8 . #\I) ( 9 . #\J) 
            (10 . #\K) (11 . #\L) (12 . #\M) (13 . #\N) (14 . #\O) 
            (15 . #\P) (16 . #\Q) (17 . #\R) (18 . #\S) (19 . #\T) 
            (20 . #\U) (21 . #\V) (22 . #\W) (23 . #\X) (24 . #\Y) 
            (25 . #\Z) 
            (26 . #\a) (27 . #\b) (28 . #\c) (29 . #\d) (30 . #\e) 
            (31 . #\f) (32 . #\g) (33 . #\h) (34 . #\i) (35 . #\j) 
            (36 . #\k) (37 . #\l) (38 . #\m) (39 . #\n) (40 . #\o) 
            (41 . #\p) (42 . #\q) (43 . #\r) (44 . #\s) (45 . #\t) 
            (46 . #\u) (47 . #\v) (48 . #\w) (49 . #\x) (50 . #\y) 
            (51 . #\z)
            (52 . #\0) (53 . #\1) (54 . #\2) (55 . #\3) (56 . #\4) 
            (57 . #\5) (58 . #\6) (59 . #\7) (60 . #\8) (61 . #\9)
            (62 . #\-) (63 . #\_))])
      (set 'index index) (set 'char char)
      (for-each setI values)
      (for-each setC values)
      (lambda (type x) (hash-table-ref (hash-table-ref table type) x))))))

(define base64url-character-table (lambda (x) (base64url-table 'index x)))
(define base64url-index-table (lambda (x) (base64url-table 'char x)))

(define (repeat/char c times)
  (define (inner l times)
    (cond 
      [(<= times 0) l]
      [else (inner (cons c l) (- times 1))]))
  (apply conc (inner '() times)))

(define (string->binary s)
  (define (char->binary c) 
    (define binary (sprintf "~B" (char->integer c)))
    (define len (string-length binary))
    (if (< len 8)
        (conc (repeat/char #\0 (- 8 len)) binary)
        binary))
  (apply conc (map char->binary (string->list s))))

(define (group-per B l)
  (define (pad-with0 xs)
    (define len (length xs))
    (define (inner l x)
      (cond 
        [(= x 0) l]
        [else (inner (cons #\0 l) (- x 1))]))
    (if (>= len B)
        xs
        (inner xs (- B len))))

  (define (inner xs acc1 acc2)
    (cond
      [(null? xs) (cons (reverse (pad-with0 acc2)) acc1)]
      [(>= (length acc2) B) (inner xs (cons (reverse acc2) acc1) '())]
      [else (inner (cdr xs) acc1 (cons (car xs) acc2))]))
  
  (if (null? l)
      '()
      (reverse (inner l '() '()))))

(define (prepend/2 l)
  (define (f xs)
    (cons #\0 (cons #\0 xs)))
  (map f l))

(define (strip-first/2 l)
  (define (f s)
    (define len (string-length s))
    (if (< len 2)
        ""
        (apply conc (map (lambda (x) (string-ref s x)) (drop (iota len) 2)))))
  (map f l))

(define (b64encode s) 
  (define (to-index x) (string->number (list->string x) 2))
  (define indices 
    (map to-index (prepend/2 (group-per 6 (string->list (string->binary s))))))
  (list->string (map base64url-character-table indices)))

(define (b64decode s) 
  (define (apply-conc x) (apply conc x))
  (define (group/8 x) (group-per 8 x))
  (define (binary->char x) (sprintf "~A" (integer->char (string->number x 2))))

  (define indices 
    (map base64url-index-table (string->list s)))
  (define binarygroups 
    (map integer->binary indices))
  (define bytegroup 
    (filter
      (lambda (x) (= 8 (length x))) 
      (group/8 (string->list (apply-conc (strip-first/2 binarygroups))))))
    
  (string-translate
    (apply-conc (map binary->char (map apply-conc bytegroup)))
    #\x00))

(define (integer->binary i)
  (define binary (sprintf "~B" i))
  (conc (repeat/char #\0 (- 8 (string-length binary))) binary))


)