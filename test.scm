
(include "base64url.scm")

(import 
  (chicken format)
  srfi-1
  srfi-13
  srfi-69
  test
  base64url)

(include "tests/run.scm")

(test-exit)